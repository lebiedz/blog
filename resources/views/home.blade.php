@extends('partials.master')
@section('meta-tags')
    <title>Michał Aleksander Burzyński // maburzyski.com</title>
    <meta name="description" content="I am a 25-year-old product manager, whom you can meet in Poznan (Poland) or Berlin (Germany). In my work I put much weight on building the reach in short period of time. Currently, I am focused on creating products such as marketplaces or SaaS.">
    <meta name="title" content="Michał Aleksander Burzyński // maburzyski.com">
@stop

@section('content')
    <main id="main-container">
        <section class="landing" id="landing">
            <div class="landing__left">
                <div class="landing__content">
                    
                    <p class="landing__content__hello">
                        HI, I’M MICHAL ALEKSANDER BURZYNSKI
                    </p>
                    <div class="landing__content__title">
                        digital product manager<br>with love for growth
                    </div>
                    <div class="landing__content__social-media">
                        <a href="https://www.linkedin.com/in/maburzynski"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        <a href="https://twitter.com/maburzynski"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    </div>
                </div>
                <lastfm></lastfm>

                <div class="scroll-down">
                    Scroll down <i class="fa fa-chevron-down fa-fw"></i>
                </div>
            </div>
            <div class="landing__right"><div class="landing__right_image"></div></div>
        </section>
        <section id="blog">
            <medium></medium>
        </section>
    </main>
@stop
	
