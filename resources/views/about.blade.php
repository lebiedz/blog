@extends('partials.master')
@section('meta-tags')
	<title>About Me // maburzynski.com</title>
    <meta name="description" content="I am a 25-year-old product manager from Poznan (Poland). I have been working in the Internet industry for over 8 years, starting as freelancer digital manager, with the weight put on generating leads for insurance trades, gambling or e-commerce.">
    <meta name="title" content="About Me // maburzynski.com">
@stop
@section('content')
	<main id="main-container" class="about">
        <section class="landing">
            <div class="landing__left">
                <div class="landing__content">
                    <img src="images/profilowe_michal.png">
                    <p class="landing__content__more-about">MORE ABOUT ME</p>
                    <div class="landing__content__title">I ❤ rapid development & growth</div>
                    <div class="landing__content__text">
                            <p>I am a 25-year-old product manager from Poznan (Poland). I have been working in the Internet industry for over 8 years, starting as freelancer digital manager, with the weight put on generating leads for insurance trades, gambling or e-commerce. Along with my growing experience in marketing, I have increasingly started to focus on topics connected with building the reach and gaining new users - which I am doing to this day.
                            </p><p>
                            I have very good technical experience due to being strong geek at high school age, which resulted in a fascination with a free bsd system, building of my own kernels and hundreds of hours spent at open bsd IRC channels. As I knew many great engineers and few marketers, the fate decided for me to become the latter.
                            </p>
                            <p>
                            Connection of those two fields, marketing + technology as well as particular love with designing (ux, user onboarding, etc.) and the experience in running my own business (7+ years) allowed me to develop further as product manager along with introducing first scalable products or services on the market. Currently, for over 3 years I have been spending increasingly more time on developing my own projects and I have started to occasionaly support businesses that need to improve their reach through nonstandard ways of building traffic.
                            </p>
                            <p>
                            In my actions, I focus on making quick prototypes, implementing and growth (after confirmation of business model).
                            </p>

                            <p>
                            Outside work: crossfit freak & post rock lover.
                            </p>
                            <p class="green">Any questions?
                            I will gladly help internet businesses that need to build great reach at global scale. <span class="email">business(at)maburzynski.com</span></p>













                    </div>
                </div>
            </div>
            <div class="landing__right"><div class="landing__right_image"></div></div>
        </section>
	</main>
@stop