@extends('partials.master')
@section('meta-tags')
	<title>{{$post->title}} // maburzynski.com</title>
    <meta name="description" content="I am a 25-year-old product manager from Poznan (Poland). I have been working in the Internet industry for over 8 years, starting as freelancer digital manager, with the weight put on generating leads for insurance trades, gambling or e-commerce.">
    <meta name="title" content="{{$post->title}} // maburzynski.com">
@stop
@section('content')
	<main id="main-container" class="about">
        <section class="landing">
            <div class="landing__left">
                <div class="landing__content">
                    <p class="landing__content__more-about">MY BLOG</p>
                    <div class="landing__content__title">{{$post->title}}</div>
                    <div class="landing__content__text">
                        {!!$post->body!!}
                    </div>
                </div>
            </div>
            <div class="landing__right"><div class="landing__right_image"></div></div>
        </section>
	</main>
@stop