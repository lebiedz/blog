@extends('partials.master')
@section('meta-tags')
    <title>Blog // maburzyski.com</title>
    <meta name="description" content="I am a 25-year-old product manager, whom you can meet in Poznan (Poland) or Berlin (Germany). In my work I put much weight on building the reach in short period of time. Currently, I am focused on creating products such as marketplaces or SaaS.">
    <meta name="title" content="Blog // maburzyski.com">
@stop
@section('content')
    <main id="main-container" class="about">
        <section class="landing">
            <div class="landing__left">
                <div class="landing__content">
                    <p class="landing__content__more-about">MY BLOG</p>
                    <div class="landing__content__post_list_wrapper">
                        @if(!isset($posts->items))
    	                    @foreach ($posts as $post)
        	                    <div class="landing__content__post_wrapper">
        		                    <div class="landing__content__title"><a href="/blog/{{$post->slug}}">{{strip_tags($post->title)}}</a></div>
        		                    <div class="landing__content__post_info"><i class="fa fa-clock-o"></i> {{$post->created_at->format('F jS, Y')}}</div>
        		                    <div class="landing__content__text">
                                    @if(strlen($post->body) > 250)
                                        {{substr(strip_tags($post->body), 0, 250) . '...'}}
                                    @else
                                        {{$post->body}}
                                    @endif
                                    </div>
        		                    <div class="landing__content__read_more"><a href="/blog/{{$post->slug}}">Read more...</a></div>
        	                    </div>
	                        @endforeach
                        @else
                            <h1>No posts to display</h1>
                        @endif
                    </div>
                </div>
            </div>
            <div class="landing__right"><div class="landing__right_image"></div></div>
        </section>
	</main>
@stop