@extends('partials.master')

@section('meta-tags')
	<title>All Posts // maburzynski.com</title>
    <meta name="title" content="All Posts // maburzynski.com">
@stop

@section('content')
<main id="main-container" class="about">
    <section class="landing">
        <div class="landing__left">
            <div class="landing__content">
                <p class="landing__content__more-about">ALL POSTS</p>
                <p class="landing__back-button"><a href="/admin"><i class="fa fa-angle-left"></i> Go back</a></p>
                <table class="landing__posts-table">
                	<thead>
                		<tr>
                			<th>Name</th>
                			<th colspan="2">Action</th>                			
                		</tr>
                	</thead>
                	<tbody>
                		@foreach($posts as $post)
                			<tr>
                				<td>{{$post->title}}</td>
                				<td><a href="/admin/edit/{{$post->id}}">Edit</a></td>
                				<td><a href="/admin/delete/{{$post->id}}">Delete</a></td>
                			</tr>
                		@endforeach
                	</tbody>
                </table>
                <p class="landing__content__more-about"><a href="/admin/add-new">ADD NEW POST</a></p>
            </div>
        </div>
        <div class="landing__right"><div class="landing__right_image"></div></div>
    </section>
</main>
@stop