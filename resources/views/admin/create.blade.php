@extends('partials.master')
@section('meta-tags')
    <title>Add new post | Blog // maburzyski.com</title>
    <meta name="title" content="Add new post | Blog // maburzyski.com">
@stop
@section('content')
    <main id="main-container" class="about">
        <section class="landing">
            <div class="landing__left">
                <div class="landing__content">
                    <p class="landing__content__more-about">ADD NEW POST</p>
                    <p class="landing__back-button"><a href="/admin"><i class="fa fa-angle-left"></i> Go back</a></p>
                    <form method="POST" action="/blog" class="create_post_form">
                        {{csrf_field()}}
                        @include('partials.errors')
                        <div class="landing__content__form-group">
                            <label for="title">Title:</label>
                            <input type="text" id="title" name="title" class="input post_title">
                        </div>
                        <div class="landing__content__form-group">
                            <label for="body">Body:</label>
                            <textarea id="body" name="body" class="input post_body"></textarea>                        
                        </div>
                        <div class="landing__content__form-group">
                            <button type="submit">Publish<i class="fa fa-chevron-right"></i></button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="landing__right"><div class="landing__right_image"></div></div>
        </section>
	</main>
@stop