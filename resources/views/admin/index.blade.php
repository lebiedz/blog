@extends('partials.master')

@section('meta-tags')
	<title>Admin panel // maburzynski.com</title>
    <meta name="title" content="Admin panel // maburzynski.com">
@stop

@section('content')
<main id="main-container" class="about">
    <section class="landing">
        <div class="landing__left">
            <div class="landing__content">
                <p class="landing__content__more-about">Hello, {{auth()->user()->name}}</p>
                <div class="landing__admin-menu">
                	<p>What would you like to do?</p>
                	<ul>
                    	<li><a href="/admin/add-new">Add new post</a></li>
                    	<li><a href="/admin/all-posts">View all posts list</a></li>
                	</ul>
                </div>
            </div>
        </div>
        <div class="landing__right"><div class="landing__right_image"></div></div>
    </section>
</main>
@stop