@extends('partials.master')

@section('meta-tags')
	<title>Log in // maburzynski.com</title>
    <meta name="title" content="Log in // maburzynski.com">
@stop

@section('content')
    <main id="main-container" class="about">
        <section class="landing">
            <div class="landing__left">
                <div class="landing__content">
                    <p class="landing__content__more-about">LOG IN</p>
                    <form method="POST" action="/login" class="create_post_form">
                        {{csrf_field()}}
                        @include('partials.errors')
                        <div class="landing__content__form-group">
                            <label for="title">E-mail:</label>
                            <input type="email" id="email" name="email" class="input post_title" required>
                        </div>
                        <div class="landing__content__form-group">
                            <label for="body">Password:</label>
                            <input type="password" id="password" name="password" class="input post_title" required>
                        </div>
                        <div class="landing__content__form-group">
                            <button type="submit">Sign in<i class="fa fa-chevron-right"></i></button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="landing__right"><div class="landing__right_image"></div></div>
        </section>
	</main>
@stop