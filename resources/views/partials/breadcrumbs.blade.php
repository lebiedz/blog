<?php // dd($_SERVER['REQUEST_URI']) ?>
@if (!($_SERVER['REQUEST_URI'] === '/' OR in_array(rtrim($_SERVER['REQUEST_URI'], '/'), $breadcrumbs->exclude())))
<div class="breadcrumbs">
    <ul>
        @foreach($breadcrumbs->create() as $index => $element)
            @if($index === 0)
                <li>{!! '<a href="/">Home</a><i class="fa fa-angle-right"></i>' !!}</li>
            @elseif($element['uri_element'] === "about")
                <li>{!! 'About Me<i class="fa fa-angle-right"></i>' !!}</li>
            @elseif($index > 0 && !$loop->last)
                <li>{!! '<a href="' . $breadcrumbs->curr_uri($index+1) . '">' . $element['title'] . '</a><i class="fa fa-angle-right"></i>' !!}</li>
            @elseif($loop->last)
                <li>{!! $element['title'] .'<i class="fa fa-angle-right"></i>' !!}</li>
            @endif
        @endforeach
    </ul>
</div>
@endif