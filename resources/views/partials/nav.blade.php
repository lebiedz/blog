        <nav class="navbar navbar-default navbar-fixed-top navbar-scroll">
            <div class="navbar__logo">
                <a href="/">{!! file_get_contents("images/logo.svg") !!}</a>
            </div>
            <div class="navbar__menu">
                <ul class="navbar__menu__list">
                    <li><a href="/" {{$_SERVER['REQUEST_URI'] == '/' ? 'class=active' : ''}}>Home</a></li>
                    <li><a href="/about" {{$_SERVER['REQUEST_URI'] == '/about' ? 'class=active' : ''}}>About Me</a></li>
                    <li><a href="/blog" {{preg_match('/blog\/?.*/', $_SERVER['REQUEST_URI']) ? 'class=active' : ''}}>Blog</a></li>
                    @if(Auth::check())
                        <li class="logout">
                            <ul>
                                <li><a href="/admin" {{preg_match('/admin\/?.*/', $_SERVER['REQUEST_URI']) ? 'class=active' : ''}}>Admin panel</a></li>
                                <li><a href="/logout">Logout</a></li>
                            </ul>
                        </li>
                    @else
                        <li class="login"><a href="/login">Login</a></li>
                    @endif
                </ul>
            </div>
        </nav>