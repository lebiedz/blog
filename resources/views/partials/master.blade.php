<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

        @yield('meta-tags')
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="google-site-verification" content="mzEtiSydSva4FNJxZLVIjSZ-g1GAjXODZyxHPwdG-EA" />
        <link href="/css/style.css" rel="stylesheet">
        <link href="/css/new.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
	</head>
<body>
    <header>
    @include('partials.nav')
    </header>
    @include('partials.breadcrumbs')

    @yield('content')    

    @include('partials.footer')
