@if (count($errors))
<div class="landing__content__form-group errors">
	<p>There were some errors with the request:</p>
	<ul>
		@foreach($errors->all() as $error)
			<li>{{$error}}</li>
		@endforeach
	</ul>
</div>
@endif