<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@home');
Route::get('about', 'PagesController@about');
Route::get('blog', 'PostsController@index');
Route::post('blog', 'PostsController@store');
Route::get('admin/add-new', 'PostsController@create');
Route::get('blog/{slug}', 'PostsController@show');
Route::get('admin/edit/{post}', 'PostsController@edit');
Route::post('admin/edit/{post}', 'PostsController@update');
Route::get('admin/delete/{post}', 'PostsController@destroy');
Route::get('login', ['as' => 'login', 'uses' => 'SessionsController@create']);
Route::post('login', 'SessionsController@store');
Route::get('logout', 'SessionsController@destroy');
Route::get('admin', 'AdminController@index');
Route::get('admin/all-posts', 'AdminController@show');