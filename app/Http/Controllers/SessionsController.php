<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionsController extends Controller
{
	public function __construct(){
		$this->middleware('guest')->only(['create', 'store']);
		$this->middleware('auth')->only(['destroy']);
	}
    public function create(){
    	return view('sessions.create');
    }
    public function store(){
    	if ( !auth()->attempt(request(['email', 'password']))){
    		return back()->withErrors('E-mail address and/or password is wrong.');
    	}
    	return redirect('admin');
    }
    public function destroy(){
    	auth()->logout();

    	return redirect()->back();
    }
}
