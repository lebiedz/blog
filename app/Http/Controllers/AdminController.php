<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class AdminController extends Controller
{
	public function __construct(){
		$this->middleware('auth');
	}
    public function index(){
    	return view('admin.index');
    }
    public function show(){
    	$posts = Post::latest()->get();
    	return view('admin.show', compact('posts'));
    }
}
