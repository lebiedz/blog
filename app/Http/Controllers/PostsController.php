<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostsController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->except(['index', 'show']);
    }
    public function index(){
    	$posts = Post::latest()->get();
    	return view('blog.index', compact('posts'));
    }
    public function create(){
    	return view('admin.create');
    }
    public function show($slug){
        $post = Post::where('slug', $slug)->first();
    	return view('blog.show', compact('post'));
    }
    public function store(){
        $this->validate(request(), [
            'title' => 'required',
            'body' => 'required'
        ]);
    	$slug = preg_replace('/\s/', '-', str_replace('-', '--', strtolower(request('title'))));
    	Post::create(['title' => request('title'), 'body' => request('body'), 'slug' => $slug]);
    	return redirect('/blog/' . $slug . '/');
    }
    public function edit($id){
        $post = Post::where('id', $id)->first();
        return view('admin.edit', compact('post'));
    }
    public function update($id){
        $post = Post::findOrFail($id);
        $this->validate(request(), [
            'title' => 'required',
            'body' => 'required'
        ]);
        if(request('title') != $post->title){
            $slug = preg_replace('/\s/', '-', preg_replace('/\-/', '--', strtolower(request('title'))));
            $post->slug = $slug;
        }
        $post->title = request('title');
        $post->body = request('body');
        $post->save();

        return view('admin.edit', compact('post'));
    }
    public function destroy($id){
        Post::where('id', $id)->delete();
        $posts = Post::latest()->get();
        return view('admin.show', compact('posts'));
    }
}
