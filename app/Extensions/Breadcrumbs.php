<?php

namespace App\Extensions;

class Breadcrumbs
{
	private $uri;
	private $uri_elements;
	private $title;
	private $curr_uri;

    public function __construct(){
        $this->uri = parse_url(url()->current(), PHP_URL_PATH);
        $this->uri_elements = explode('/', $this->uri);
    }

    private function home(){

    }

	public function create(){
        $this->title = array_map(
        	function($element){
        		return ucfirst(str_replace('.', '-', str_replace('-', ' ', str_replace('--','.', $element))));
        	}, $this->uri_elements);
        foreach($this->uri_elements as $index => $element){
        	$breadcrumbs[] = ['uri_element' => $element, 'title' => $this->title[$index]];
        }

        return $breadcrumbs;
	}
	
	public function curr_uri($id){
		$this->curr_uri = join('/', array_slice($this->uri_elements, 0, $id));
		return $this->curr_uri;
	}

    public function exclude(){
        $excluded = [
            '/\/admin.*/',
            '/\/login/'
        ];
        foreach($excluded as $exclude){
        preg_match($exclude, rtrim($this->uri, '/'), $matches);
        if(!empty($matches)){
            break;
            }
        }
        return $matches;
    }
}